const fs = require('fs-extra'),
	os = require('os'),
	path = require('path');

const packageJson = require('../package.json');

const fotnoSchema = require('@fontoxml/fontoxml-development-tools-module-schema');
const XSD_DATE_TYPES = [
		'xsd:date',
		'xsd:time',
		'xsd:dateTime',
		'xsd:duration',
		'xsd:gDay',
		'xsd:gMonth',
		'xsd:gMonthDay',
		'xsd:gYear',
		'xsd:gYearMonth'
	],
	XSD_NUMERIC_TYPES = [
		'xsd:byte',
		'xsd:decimal',
		'xsd:short',
		'xsd:int',
		'xsd:integer',
		'xsd:long',
		'xsd:negativeInteger',
		'xsd:nonNegativeInteger',
		'xsd:noPositiveInteger',
		'xsd:positiveInteger',
		'xsd:unsignedByte',
		'xsd:unsignedShort',
		'xsd:unsignedInt',
		'xsd:unsignedLong'
	];

function attributeSpecIsMatch(str, element, attribute) {
	return str === element.localName
		|| str === '@' + attribute.localName
		|| str === element.localName + '@' + attribute.localName;
}

// Vitaliy Ulantikov @ http://stackoverflow.com/questions/7225407/convert-camelcasetext-to-camel-case-text
function tryCreateHumanReadableLabel(camelCase) {
	if (!camelCase) {
		return '';
	}

	var pascalCase = camelCase.charAt(0).toUpperCase() + camelCase.substr(1);
	return pascalCase
		.replace(/([a-z])([A-Z])/g, '$1 $2')
		.replace(/([A-Z])([A-Z][a-z])/g, '$1 $2')
		.replace(/([a-z])([0-9])/gi, '$1 $2')
		.replace(/([0-9])([a-z])/gi, '$1 $2');
}

function isAttributeConfigurationEqual (a, b) {
	if(!['localName', 'label', 'isRequired', 'display'].every(prop => a[prop] === b[prop]))
		return false;

	if(!a.properties)
		return true;

	if(!b.properties)
		return false;

	if(a.type !== b.type)
		return false;

	if(a.properties.options) {
		if(!b.properties.options)
			return false;

		if(a.properties.options.length !== b.properties.options.length)
			return false;

		if(!a.properties.options.every(allowedValue => b.properties.options.indexOf(allowedValue) >= 0))
			return false;
	}

	return true;
}

module.exports = fotno => {
	function attributesEditorCommand (req, res) {
		let verbose = req.options.verbose;

		res.caption('fotno ' + req.command.name);

		if (req.options.whitelist.length) {
			res.debug('Whitelist: ' + req.options.whitelist.join(', '));
		}
		if (req.options.blacklist.length) {
			res.debug('Blacklist: ' + req.options.blacklist.join(', '));
		}
		if (req.options['read-only'].length) {
			res.debug('Read-only: ' + req.options['read-only'].join(', '));
		}
		if (req.options['blacklist-xsd-types'].length) {
			res.debug('Blacklist XSD types: ' + req.options['blacklist-xsd-types'].join(', '));
		}

		let schemaSummaries = req.options.schemas.length
				? req.options.schemas.map(schemaNameFragment => fotnoSchema.getClosestMatchingSchemaSummary(req.fdt.editorRepository.path, schemaNameFragment))
				: fotnoSchema.getSchemaSummaries(req.fdt.editorRepository.path);

		let elements = schemaSummaries
			.reduce((elements, schemaSummary) => elements.concat(schemaSummary.getAllElements()), []);

		let configuration = schemaSummaries
			.reduce((attributes, schemaSummary) => attributes.concat(schemaSummary.getAllAttributes()), [])
			.map(attribute => {
				return {
					elements: attribute.getElements()
						.filter(element => {
							if(req.options.blacklist.some(blacklisted => attributeSpecIsMatch(blacklisted, element, attribute))) {
								verbose && res.debug('Skipping "' + element.localName + "@" + attribute.localName + '", in blacklist.');
								return false;
							}

							const elementNamespace = element.localName.includes(':') ? element.localName.split(':')[0] : null;
							if(elementNamespace && req.options['blacklist-ns'].some(namespace => namespace === elementNamespace)) {
								verbose && res.debug('Skipping "' + element.localName + "@" + attribute.localName + '", in namespace blacklist.');
								return false;
							}

							if(req.options.whitelist.length && !req.options.whitelist.some(whitelisted => attributeSpecIsMatch(whitelisted, element, attribute))) {
								verbose && res.debug('Skipping "' + element.localName + "@" + attribute.localName + '", not in witelist.');
								return false;
							}

							return true;
						})
						.sort((a, b) => a.localName.localeCompare(b.localName)),
					attribute: attribute
				};
			})
			.filter(entry => !!entry.elements.length)
			.map(entry => {
				let attribute = entry.attribute;

				let xsdType = attribute.typeNamespace === 'http://www.w3.org/2001/XMLSchema'
					? 'xsd:' + attribute.typeLocalName
					: null;

				if (req.options['blacklist-xsd-types'].length && req.options['blacklist-xsd-types'].indexOf(xsdType) >= 0) {
					verbose && res.debug('Skipping @' + attribute.localName + ', in xsd types blacklist ("' + xsdType + '").');
					return entry;
				}

				let attributeConfiguration = {
					localName: attribute.localName,
					label: tryCreateHumanReadableLabel(attribute.localName),
					isRequired: attribute.use === 'required'
				};

				let isReadOnly = req.options['read-only'].some(readonly => attributeSpecIsMatch(readonly, { localName: '_' }, entry.attribute));

				if(isReadOnly) {
					verbose && res.debug(`Configure @${attribute.localName} as read-only.`);
					attributeConfiguration.display = 'readonly';
				}

				else if (xsdType && XSD_DATE_TYPES.indexOf(xsdType) >= 0) {
					verbose && res.debug(`Configure ${attribute.localName} as date.`);
					attributeConfiguration.display = "date";
				}

				else if (xsdType && XSD_NUMERIC_TYPES.indexOf(xsdType) >= 0) {
					verbose && res.debug(`Configure @${attribute.localName} as input with type number.`);
					attributeConfiguration.display = 'input';
					attributeConfiguration.properties = {
						type: 'number'
					};
				}

				else if(attribute.allowedValues && attribute.allowedValues.length) {
					verbose && res.debug(`Configure @${attribute.localName} as select with options "${attribute.allowedValues.join(', ')}"`);
					attributeConfiguration.display = 'select';
					attributeConfiguration.properties = {
						options: attribute.allowedValues.map(allowedValue => ({
							label: allowedValue.charAt(0).toUpperCase() + allowedValue.slice(1),
							value: allowedValue
						}))
					};
				}

				else {
					verbose && res.debug(`Unknown xsdType for  @${attribute.localName}: "${xsdType}"`);
					attributeConfiguration.display = 'input';
					attributeConfiguration.properties = {
						type: 'text'
					};
				}

				return Object.assign(entry, {
					configuration: attributeConfiguration
				})

			})
			.filter(entry => !!entry.configuration)

			// Concatenate element selectors for attributes that have an equal configuration
			// - Also drop the "attributes" property of an entry because it is no longer always accurate
			.reduce((entries, entry) => {
				let duplicateEntry = entries.find(e => isAttributeConfigurationEqual(e.configuration, entry.configuration));

				if (duplicateEntry) {
					verbose && res.debug(`Merging config of @${duplicateEntry.localName} of ${duplicateEntry.elements.length} elements with another ${entry.elements.length}`);
					duplicateEntry.elements = duplicateEntry.elements.concat(entry.elements);
				} else {
					entries.push({
						localName: entry.attribute.localName,
						elements: entry.elements,
						configuration: entry.configuration
					});
				}

				return entries;
			}, [])

			// Reduce to an object with the proper selectors
			.reduce(req.options['no-xpath']
				? function legacyMergeAttributeConfig (config, entry) {
					entry.elements.forEach(element => {
						let selector = 'self::' + element.localName;

						if(!config[selector])
							config[selector] = {};

						config[selector][entry.localName] = entry.configuration;
					});
					return config;
				}
				: function xPathMergeAttributeConfig(config, entry) {
					let selector = null;

					if(entry.elements.length === elements.length)
						selector = '*';

					else if(entry.elements.length === 1)
						selector = entry.elements[0].localName;

					else
						selector = `self::*[name() = ('${entry.elements.map(e => e.localName).join(`','`)}')]`;

					if(!config[selector])
						config[selector] = {};

					config[selector][entry.localName] = entry.configuration;

					return config;
				},
				{
					"false() and fotnoAttributesEditorGenerator": {
						"options": {
							"localName": "__fotnoAttributesEditorGeneratorOptions",
							"label": "Options for fotno attributes editor configuration generator",
							"display": "readonly",
							"properties": {
								"fotnoModule": packageJson.name,
								"fotnoModuleVersion": packageJson.version,
								"fotnoVersion": fotno.version, // Since 3.8.1
								"fotnoArgv": process.argv
							}
						}
					}
				});

		let exportJson = JSON.stringify(configuration, null, '	') + os.EOL,
			exportPath = path.resolve(process.cwd(), req.options.export);

		if (fs.existsSync(exportPath)) {
			res.notice(`Overwriting existing file ${exportPath}`);
		} else {
			res.debug(`Creating new file ${exportPath}`);
		}

		return new Promise((resolve, reject) => {
				fs.outputFile(exportPath, exportJson, err => err ? reject(err) : resolve());
			})
			.then(() => {
				res.success(`Finished generating properties panel configuration.`);
			});
	}

	fotno.registerCommand('generate-properties-panel')
		.setController(attributesEditorCommand)
		.addOption(new fotno.MultiOption('whitelist')
			.setShort('w')
			.setDefault([], true)
			.setDescription('Only use specific elements and/or attributes, space separated in the form of "element", "element@attribute" or "@attribute".')
		)
		.addOption(new fotno.MultiOption('blacklist')
			.setShort('b')
			.setDefault([], true)
			.setDescription('Do not use specific elements and/or attributes, space separated in the form of "element", "element@attribute" or "@attribute".')
		)
		.addOption(new fotno.MultiOption('blacklist-xsd-types')
			.setDefault([], true)
			.setDescription('Do not use attributes of a certain XSD type.')
		)
		.addOption(new fotno.MultiOption('blacklist-ns')
			.setDefault([], true)
			.setDescription('Do not use attributes for elements of a certain namespace')
		)
		.addOption(new fotno.MultiOption('read-only')
			.setDefault([], true)
			.setShort('r')
			.setDescription('Make specific elements and/or attributes read-only, space separated in the form of "@attribute".')
		)
		.addOption('no-xpath', 'X', 'Do not use xPath to specify the element name(s) for an attribute. Not sure why you would want to use this option, may be removed without notice at any time.')
		.addOption('export', 'E', 'Name of the file to export to. Will overwrite existing files. Only JSON is supported, so (in contrary to exporting a table), the file extension is ignored.', true)
		.addOption('verbose', 'v', 'Verbose logging; Show skipped attributes.', false)
		.addOption(new fotno.MultiOption('schemas')
			.setShort('s')
			.setDescription('Specify (a part of) the path to one or more of the schemas. Leave empty for all schemas.')
		)
		.setDescription('Generate configuration for the attribute editor');
};
